import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
       /*
      // Speed test 4 tries 4 sizes
      int[] graphSize = new int[] {25, 625, 3125, 3750};
      Vertex middleVertex;
      long stime;
      long ftime;

      for (int tryN = 1; tryN < 5; tryN++) {
         System.out.printf("Try No: %d%n", tryN);
         for (int i = 0; i < graphSize.length; i++) {
            int size = graphSize[i];
            Graph testG = new Graph(String.format("Graph with %s vertexes and %s edges",
                    size, size * (size - 1) / 2));
            testG.createRandomSimpleGraph(size, size * (size - 1) / 2);
            middleVertex = testG.first;
            for (int j = 1; j < size / 2; j++) {
               middleVertex = middleVertex.next;
            }
            stime = System.nanoTime();
            testG.removeVertexAndConnectedArcs(middleVertex);
            ftime = System.nanoTime();
            System.out.printf("%s; time: %dms%n", testG.id, (ftime - stime) / 1000000);
         }
      }
      */
   }

   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 5000)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Remove given Vertex object with connected (incoming and outgoing) arcs from given Graph
       * Throws Runtime exception if Vertex object was not found among graph elements
       * @param vertexToRemove vertex object to remove
       */
      public void removeVertexAndConnectedArcs(Vertex vertexToRemove) {

         Boolean foundVertexObject = false;     // Check if Vertex exist in graph, throws not found exception
         Vertex currentVertex = first;
         Arc currentArc;
         Vertex previousVertex = null;
         Arc previousArc = null;

         // Loop through vertexes
         while (currentVertex != null) {
            // Remove vertex and reorder vertex connections
            if (currentVertex == vertexToRemove) {
               foundVertexObject = true;
               currentVertex = currentVertex.next;
               if (previousVertex == null) {
                  first = currentVertex;
               } else {
                  previousVertex.next = currentVertex;
               }
               continue;
            }

            currentArc = currentVertex.first;
            // Loop through arcs of current vertex
            while (currentArc != null) {
               // Remove arc and reorder arc connections
               if (currentArc.target == vertexToRemove) {
                  foundVertexObject = true;
                  currentArc = currentArc.next;
                  if (previousArc == null) {
                     currentVertex.first = currentArc;
                  }
                  else {
                     previousArc.next = currentArc;
                  }
                  continue;
               }
               previousArc = currentArc;
               currentArc = currentArc.next;
            }
            previousArc = null;                    // Reset arc chain. Each vertex has it's own chain
            previousVertex = currentVertex;        // Analysed Vertex to previous
            currentVertex = currentVertex.next;    // Get next Vertex in chain
         }

         // Vertex object not found in graph
         if (!foundVertexObject) {
            throw new RuntimeException(String.format("Given Vertex object (%s) does not exist in graph (%s)",
                    vertexToRemove.toString(), id));
         }
      }
   }
} 

