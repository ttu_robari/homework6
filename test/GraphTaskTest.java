import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (expected=RuntimeException.class)
   public void removeEmptyGraph() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Vertex vertex = graphTask.new Vertex("Test");
      graph.removeVertexAndConnectedArcs(vertex);
   }

   @Test (expected=RuntimeException.class)
   public void removeVertexNotInGraph() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      graph.createRandomSimpleGraph(6, 9);

      GraphTask.Vertex vertex = graphTask.new Vertex("Test");
      graph.removeVertexAndConnectedArcs(vertex);
   }

   @Test (timeout=20000)
   public void removeFirst() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Graph graph2 = graphTask.new Graph("Test");

      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      GraphTask.Vertex v31 = graph2.createVertex("v3");
      GraphTask.Vertex v21 = graph2.createVertex("v2");

      graph.createArc("v1v2", v1, v2);
      graph.createArc("v1v3", v1, v3);
      graph.createArc("v2v1", v2, v1);
      graph.createArc("v2v3", v2, v3);
      graph.createArc("v3v1", v3, v1);
      graph.createArc("v3v2", v3, v2);

      graph2.createArc("v2v3", v21, v31);
      graph2.createArc("v3v2", v31, v21);

      graph.removeVertexAndConnectedArcs(v1);

      assertEquals (graph2.toString(), graph.toString());
   }

   @Test (timeout=20000)
   public void removeMiddle() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Graph graph2 = graphTask.new Graph("Test");

      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      GraphTask.Vertex v31 = graph2.createVertex("v3");
      GraphTask.Vertex v11 = graph2.createVertex("v1");

      graph.createArc("v1v2", v1, v2);
      graph.createArc("v1v3", v1, v3);
      graph.createArc("v2v1", v2, v1);
      graph.createArc("v2v3", v2, v3);
      graph.createArc("v3v1", v3, v1);
      graph.createArc("v3v2", v3, v2);

      graph2.createArc("v1v3", v11, v31);
      graph2.createArc("v3v1", v31, v11);

      graph.removeVertexAndConnectedArcs(v2);

      assertEquals (graph2.toString(), graph.toString());
   }

   @Test (timeout=20000)
   public void removeLast() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Graph graph2 = graphTask.new Graph("Test");

      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      GraphTask.Vertex v31 = graph2.createVertex("v3");
      GraphTask.Vertex v21 = graph2.createVertex("v2");

      graph.createArc("v1v2", v1, v2);
      graph.createArc("v1v3", v1, v3);
      graph.createArc("v2v1", v2, v1);
      graph.createArc("v2v3", v2, v3);

      graph2.createArc("v2v3", v21, v31);

      graph.removeVertexAndConnectedArcs(v1);

      assertEquals (graph2.toString(), graph.toString());
   }

   @Test (timeout=20000)
   public void removeOnlyVertex() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Graph graph2 = graphTask.new Graph("Test");

      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      GraphTask.Vertex v31 = graph2.createVertex("v3");
      GraphTask.Vertex v21 = graph2.createVertex("v2");
      GraphTask.Vertex v11 = graph2.createVertex("v1");

      graph.createArc("v1v2", v1, v2);
      graph.createArc("v1v3", v1, v3);
      graph.createArc("v2v1", v2, v1);
      graph.createArc("v2v3", v2, v3);
      graph.createArc("v3v1", v3, v1);
      graph.createArc("v3v2", v3, v2);

      graph2.createArc("v1v2", v11, v21);
      graph2.createArc("v1v3", v11, v31);
      graph2.createArc("v2v1", v21, v11);
      graph2.createArc("v2v3", v21, v31);
      graph2.createArc("v3v1", v31, v11);
      graph2.createArc("v3v2", v31, v21);

      graph.removeVertexAndConnectedArcs(v4);

      assertEquals (graph2.toString(), graph.toString() + "S");
   }

   @Test (timeout=20000)
   public void removeGraphVertexesOnly() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("Test");
      GraphTask.Graph graph2 = graphTask.new Graph("Test");

      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      GraphTask.Vertex v41 = graph2.createVertex("v4");
      GraphTask.Vertex v21 = graph2.createVertex("v2");
      GraphTask.Vertex v11 = graph2.createVertex("v1");

      graph.removeVertexAndConnectedArcs(v3);

      assertEquals (graph2.toString(), graph.toString());
   }
}

